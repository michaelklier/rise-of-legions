# README

This is the respository of the Fmod project for  [Rise Of Legions](https://store.steampowered.com/app/748940/Rise_of_Legions/) by [Broken Games](http://www.brokengames.de/), 
a hybrid of MOBA, tower defense and deckbuilding - with fast-paced, easy-to-pickup tug-of-war strategy,
that is now Open Source and available on [Github](https://github.com/BrokenGamesUG/rise-of-legions).

## Version Compatibility
This project was made using Fmod Version 1.10.01. If you want to replace sounds with your own, you need 
to use this exact FMOD version. Keep in mind that updating to newer versions also requires an update of the Fmod SDK
used in the custom [game engine](https://github.com/BrokenGamesUG/delphi3d-engine) of Rise Of Legions.

## License
All the audio files included in this project are licensed under a [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/).

## Credit
Music by Julian Colbus www.mediacracy-music.com

Sound Effects by Michael Klier www.michaelklier.studio
